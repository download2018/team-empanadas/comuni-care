<div class="newReport">
	<form method="post" id="mForm" enctype="multipart/form-data">
		<div class="sTitle">Tipologia segnalazione</div><br/>
		<div class="cat_container">
			<div class="categories img1" data-id="1"></div>
			<div class="categories img2" data-id="2"></div>
			<div class="categories img3" data-id="3"></div>
			<div class="categories img4" data-id="4"></div>
			<div class="categories img5" data-id="5"></div>
			<div class="categories img6" data-id="6"></div>
		</div><br/>
		<input type="hidden" name="category" id="category" value="0" />
		<div class="sTitle">Dove</div>
		<input type="text" name="where" class="roundInput" style="width: calc(100% - 40px);" /><br/>
		<div class="sTitle">Titolo</div>
		<input type="text" name="title" class="roundInput" onchange="checkDuplicate(this);" style="width: calc(100% - 40px);" /><br/>
		<span id="suggester">&nbsp;</span><br/>
		<div class="sTitle">Descrizione</div><br/>
		<textarea class="roundArea" name="description"></textarea><br/>
		<div class="sTitle">Allega una fotografia</div><br/>
		<input type="file" name="attachment" /><br/>
		<br/>
		<div class="submitBtn">Invia</div>
		$$error$$
	</form>
</div>