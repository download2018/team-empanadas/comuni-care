<?php
class Database {
    /** @var mysqli $link */
    protected $link;

    /**
     * xLibDB constructor.
     * @param mysqli $link
     */
    public function __construct($link = null) {
        if (is_array($link))
            $this->connect($link);
        else
            $this->link = $link;
    }
    public function __destruct() {
        if ($this->isConnected()) {
            if ($this->link->ping())
                $this->link->close();
        }
        unset($this->link);
    }
    public function connect($auth) {
        $this->link = mysqli_connect($auth['host'], $auth['user'], $auth['pass'], $auth['name']);
    }
    public function isConnected() {
        return (!!$this->link);
    }
    public function query($q, $isSelect=false, $onlyFirst=false) {
        if (!$this->isConnected())
            return false;
        $rows = array();
        $res = mysqli_query($this->link, $q);
        if (!$isSelect || !$res)
            return !!$res;
        if ($onlyFirst)
            return mysqli_fetch_array($res, MYSQLI_ASSOC);
        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC))
            array_push($rows, $row);
        return $rows;
    }
    public function escape($value) {
        $type = gettype($value);
        if ($type === 'string')
            $value = "'".$this->link->real_escape_string($value)."'";
        elseif ($type !== 'integer' && $type !== 'double')
            $value = "''";
        return $value;
    }
    public function real_escape($value) {
        return $this->link->real_escape_string($value);
    }
    private function getProjection($projection) {
        return (count($projection) > 0 ? implode(',', $projection) : '*');
    }
    private function buildWhere($where=array()) {
        $q = " WHERE ";
        foreach ($where as $key=>$val) {
            $val = $this->escape($val);
            $q .= "$key=$val AND ";
        }
        return (count($where) > 0 ? substr($q, 0, -5) : "");
    }
    private function checkLimit($limit) {
        $t = gettype($limit);
        $f = function($n) { return (is_numeric($n) && ((int)$n) > 0 && ((int)$n) < PHP_INT_MAX); };
        if ($t === 'integer' || is_numeric($limit)) {
            return $f($limit);
        } elseif ($t === 'string') {
            $comma = explode(',', $limit);
            return (count($comma) === 2 && $f($comma[0]) && $f($comma[1]));
        } else
            return false;
    }
    private function checkOrder(&$order) {
        if (gettype($order) !== 'string')
            return false;
        $order = strtoupper(trim($order));
        $e = explode(' ', $order);
        $valid = array('ASC', 'DESC');
        if (count($e) !== 2 || !in_array($e[1], $valid, true))
            return false;
        return SecureString($e[0], StringFilters::AZ);
    }
    protected function buildSelect($from, $where=array(), $projection=array(), $opts=array()) {
        $p = $this->getProjection($projection);
        $q = "SELECT $p FROM $from".$this->buildWhere($where);
        if (count($opts) > 0) {
            if (isset($opts['order']) && $this->checkOrder($opts['order']))
                $q .= $opts['order'];
            if (isset($opts['limit']) && $this->checkLimit($opts['limit']))
                $q .= " LIMIT " . $opts['limit'];
        }
        return $q;
    }
    protected function buildInsert($into, $data) {
        $q = "INSERT INTO $into ";
        $cols = "";
        $values = "";
        foreach ($data as $col=>$val) {
            $cols .= "$col,";
            $val = $this->escape($val);
            $values .= "$val,";
        }
        $cols = substr($cols, 0, -1);
        $values = substr($values, 0, -1);
        $q .= "($cols) VALUES ($values);";
        return $q;
    }
    protected function buildUpdate($table, $set, $where=array()) {
        $q = "UPDATE $table SET ";
        foreach ($set as $key=>$value) {
            $value = $this->escape($value);
            $q .= "$key=$value,";
        }
        $q = substr($q, 0, -1);
        $q .= $this->buildWhere($where);
        return $q;
    }
    public static function join($tables, $type = 'INNER') {
        $jq = '';
        $first = true;
        foreach ($tables as $t1=>$t2) {
            $tab1 = explode('.', $t1, 2);
            $tab2 = explode('.', $t2, 2);
            $buf = " $type JOIN ".$tab2[0]." ON ".$tab1[0].".".$tab1[1]."=".$tab2[0].".".$tab2[1];
            if ($first) {
                $jq .= $tab1[0].$buf;
                $first = false;
            } else
                $jq = "($jq)$buf";
        }
        return $jq;
    }
    public function transaction($flags = 2) {
        return $this->link->begin_transaction($flags);
    }
    public function commit() {
        return $this->link->commit();
    }
    public function rollback() {
        return $this->link->rollback();
    }
    public function listJoin($tableJoins, $filter=array(), $onlyFirst = false, $type = "INNER") {
        $joinQuery = $this->join($tableJoins, $type);
        $q = "SELECT * FROM " . $joinQuery . " " . $this->buildWhere($filter) . ';';
        return $this->query($q, true, $onlyFirst);
    }
    public function listTable($tableName, $filter=array(), $projection=array(), $onlyFirst = false) {
        $q = $this->buildSelect($tableName, $filter, $projection) . ($onlyFirst ? ' LIMIT 1;' : ';');
        return $this->query($q, true, $onlyFirst);
    }
    public function lastInsertId() {
        return $this->link->insert_id;
    }
    public function multiInsert($into, $data = array(), $transaction = true) {
        if ($transaction)
            $this->transaction();
        $f = true;
        foreach ($data as $row) {
            $query = $this->buildInsert($into, $row);
            $f = $f && $this->query($query);
            if (!$f && $transaction)
                break;
        }
        if ($transaction) {
            if ($f)
                return $this->commit();
            else
                $this->rollback();
        }
        return $f;
    }
    public function encodeID($key, $id, $table) {
        $t = DBTables::findCode($table);
        if ($t === -1) return false;
        $data = chr($t) . pack('V', $id) . pack('V', time());
        $hash = hash_hmac('sha256', $data, DB_SERVER_HMAC, true);
        $data = substr($hash, 0, 23) . $data;
        $data = aes_encrypt(true, $data, $key);
        return encode($data);
    }
    public function decodeID($key, $code) {
        $data = decode($code);
        $data = aes_encrypt(false, $data, $key);
        if (gettype($data) === 'string' && strlen($data) !== 32)
            return false;
        $hash = substr($data, 0, 23);
        $data = substr($data, 23);
        $hmac = hash_hmac('sha256', $data, DB_SERVER_HMAC, true);
        $hmac = substr($hmac, 0, 23);
        if ($hmac !== $hash)
            return false;
        $table = DBTables::valueByCode(ord($data[0]));
        $id = unpack('V', substr($data, 1, 4));
        $time = unpack('V', substr($data, 5, 4));
        $time = $time[1]; $id = $id[1];
        return (array('id' => $id, 'table' => $table, 'time' => $time));
    }
}
class Enum {
    public static function dump() {
        $calledClass = get_called_class();
        $r = new ReflectionClass($calledClass);
        return $r->getConstants();
    }
    public static function findCode($key) {
        $data = self::dump();
        $i = 0;
        foreach ($data as $k=>$v) {
            if ($k === $key || $v === $key)
                return $i;
            $i++;
        }
        return -1;
    }
    public static function valueByCode($code) {
        $data = self::dump();
        if (!is_int($code) || ($code < 1 && $code > count($data)))
            return null;
        return array_values($data)[--$code];
    }
}
class DBTables extends Enum {
    const User = "Users";
    const Department = "Departments";
    const Permission = "Permissions";
    const DepartmentChange = "DepartmentChange";
    const Report = "Reports";
    const Badge = "Badges";
    const UserBadge = "UserBadges";
    const Feedback = "Feedbacks";
}
class UserTypes extends Enum {
    const User = 1;
    const Functionary = 2;
    const SuperUser = 3;
}
class ReportStatus extends Enum {
    const Open = 1;
    const Accepted = 2;
    const InProgress = 3;
    const Solved = 4;
    const Rejected = 5;
}
class BadgeType extends Enum {
    const Silver = 1;
    const Gold = 2;
}