<?php
class StringFilters {
    const Separators = array(45 => 45, 95 => 95);
    const AZ = array(65 => 90);
    const _09 = array(48 => 57);
    const Space = array(32 => 32);
    public static function Join(...$vars) {
        $o = array();
        foreach ($vars as $var)
            foreach ($var as $min => $max)
                $o[$min] = $max;
        return $o;
    }
}
function h($s) {
    return htmlentities($s, ENT_QUOTES);
}
function p($arr, $key, $check) {
    if (isset($arr[$key])) {
        $v = $arr[$key];
        if ($check($v))
            return $v;
    }
    return false;
}
function encode($str) {
    return str_replace(array('+','/','='),array('-','_',''),base64_encode($str));
}
function decode($mb64) {
    return base64_decode(str_replace(array('-','_'),array('+','/'),$mb64));
}
function checkString($str, $min=0, $max=-1) {
    if (gettype($str) !== 'string') return false;
    $len = strlen($str);
    if ($len < $min) return false;
    return !($max !== -1 && $len > $max);
}
function hasChars($str, $chars) {
    for ($i = 0; $i < strlen($str); $i++)
        if (in_array($str[$i], $chars, true))
            return true;
    return false;
}
function SecureString($str, $filter, $cleanChars = false) {
    if (!checkString($str))
        return ($cleanChars ? '' : false);
    $o = '';
    $len = strlen($str);
    if ($len > 0) {
        for ($i = 0; $i < $len; $i++) {
            $c = ord($str[$i]);
            $isValid = false;
            foreach ($filter as $min => $max) {
                if ($min <= $c && $c <= $max) {
                    if ($cleanChars)
                        $o .= $str[$i];
                    else
                        $isValid = true;
                }
            }
            if (!$cleanChars && !$isValid)
                return false;
        }
    }
    return ($cleanChars ? $o : true);
}
function isValidBase64($b64) {
    static $banCache = null;
    if (is_null($banCache))
        $banCache = StringFilters::Join(StringFilters::AZ, StringFilters::_09, StringFilters::Separators);
    return (bool) SecureString(strtoupper($b64), $banCache, false);
}
function upload() {
    $exts = array("jpg","jpeg","gif","rar","zip","png");
    $dir = ROOTDIR . "uploads/";
    $ext = basename($_FILES['attachment']['name']);
    $p = strrpos($ext, '.');
    if ($p == false) //type confusion is useful now :D
        return false;
    $ext = substr($ext, $p + 1);
    if (!in_array($ext, $exts, true))
        return false;
    $name = bin2hex(random_bytes(9)) . '.' . $ext;
    if (!move_uploaded_file($_FILES['attachment']['tmp_name'], $dir.$name))
        return false;
    return $name;
}
function directDownload($name, $data, $isFile=false) {
    header("Content-Type: application/octet-stream");
    header("Content-Transfer-Encoding: binary");
    header('Content-Disposition: attachment; filename="'.$name.'"');
    header("Expires: 0");
    header("Cache-Control: no-cache, must-revalidate");
    header("Cache-Control: private");
    header("Pragma: public");
    $len = ($isFile ? filesize($data) : strlen($data));
    header("Content-Length: $len");
    if ($isFile)
        readfile($data);
    else
        echo $data;
    exit;
}
function redirect($url) {
    header("Location: $url");
    exit;
}
function includeBootstrap(&$page) {
    $page->pushToHeadTag('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">');
    $page->pushToBodyTag('<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>');
}
function countFeedbacks(&$report) {
    $c1 = 0;
    $c2 = 0;
    if (isset($report['feedbacks'])) {
        $f = json_decode($report['feedbacks'], true);
        foreach ($f as $feedback) {
            if ($feedback['feedbackType'] == 1)
                $c1++;
            elseif ($feedback['feedbackType'] == 2)
                $c2++;
        }
    }
    $report['bother'] = $c1;
    $report['danger'] = $c2;
};