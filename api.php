<?php
require 'inc/engine.php';

function out($d) { return json_encode($d); }
function outBool($bool) {
    if ($bool)
        out(['success' => true]);
    else
        out(['success' =>false]);
}
$db = new Database();
connect($db);
$api = new API($db);

$input = file_get_contents('php://input');
if (strlen($input) === 0) exit;
$data = json_decode($input, true);
if (!isset($data['action'])) exit;
switch ($data['action']) {
    case 'checkDuplicate':
        $dup = $api->FindDuplicates($data['title']);
        if (is_array($dup)) {
            echo 'Trovato possibile duplicato: <a href="?p=report&id='.$dup['id'].'" target="_blank">'.$dup['title'].'</a>';
        }
        break;
    case 'login':
        outBool($api->Login($data['mail'], $data['pass']));
        break;
    case 'reportsList':
        $reports = $api->ListReports();
        echo json_encode($reports);
        break;
    case 'report':
        $id = (int) $data['id'];
        $report = $api->GetReport($id);
        echo json_encode($report);
        break;
    case 'profile':
        $id = (int) $data['id'];
        $profile = $api->GetUserProfile(array('id' => $id));
        echo json_encode($profile);
        break;
    default:
        echo json_encode(['error' => 'Unknown Command']);
        break;
}