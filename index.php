<?php
require 'inc/engine.php';

$db = new Database();
connect($db);
$api = new API($db);
$page = new xPage();

$p = 'login';
if (isset($_GET['p'])) {
    $allow = xPage::AvailablePages();
    if (in_array($_GET['p'], $allow, true))
        $p = $_GET['p'];
}

if ($api->LoadSession()) {
    if (in_array($p, array('login', 'register'), true))
        $p = 'index';
    $isFunctionary = ($api->CurrentUser['userType'] >= UserTypes::Functionary);
    $isSuperUser = ($api->CurrentUser['userType'] == UserTypes::SuperUser);
    $page->loadBody($p);
    $id = false;
    if (isset($_GET['id']) && is_numeric($_GET['id']))
        $id = (int)$_GET['id'];
    switch ($p) {
        case "index":
            if ($isFunctionary)
                redirect('?p=dashboard');
            else
                redirect('?p=reportslist');
            break;
        case "reportslist":
            includeBootstrap($page);
            $page->setTitle('Segnalazioni');
            $isMap = (isset($_GET['view']) && $_GET['view'] === 'map');
            if ($isMap) {
                $page->body->setVariables(array('data' => '<iframe width="800" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiMjk4ZGFiNmUtODI0Mi00MDNiLTgxOWUtMTYwYjMwNDc2MzM3IiwidCI6IjE5OGU2MTU2LTExYjctNDk3NS05ZGRhLThlYWI3MTAwMGZiYyIsImMiOjh9" frameborder="0" allowFullScreen="true"></iframe>'));
                break;
            }
            $reports = null;
            $where = array();
            if (!$isSuperUser && $isFunctionary)
                $reports = $api->ListReports(array('departmentId' => $api->CurrentUser['myDepartment']));
            elseif (!$isFunctionary)
                $reports = $api->ListNotRejectedReports();
            else
                $reports = $api->ListReports();

            if (!is_array($reports))
                $page->panic();

            $page->body->setVariables(array('data' => xPage::ToHtmlTable($reports)));
            break;
        case "createreport":
            if ($isFunctionary)
                $page->panic();
            $page->setTitle('Nuova segnalazione');
            $err = '';
            do {
                if (isset($_POST['title'])) {
                    $category = $_POST['category'];
                    if (!is_numeric($category)) {
                        $err = 'Tipologia non valida';
                        break;
                    }
                    $category = (int)$category;
                    if ($category < 1 || $category > 6) {
                        $err = 'Seleziona una tipologia';
                        break;
                    }
                    $title = h($_POST['title']);
                    $where = h($_POST['where']);
                    $desc  = h($_POST['description']);
                    $photo = '';
                    if ($_FILES["attachment"]["error"] === 0) {
                        $up = upload();
                        if (is_string($up))
                            $photo = $up;
                        else {
                            $err = "L'immagine caricata non &egrave; in un formato supportato";
                            break;
                        }
                    }
                    $q = array(
                        'userId' => $api->CurrentUser['id'],
                        'departmentId' => $category,
                        'creationDate' => time(),
                        'title' => $title,
                        'description' => $desc,
                        'locationStr' => $where
                    );
                    if (!empty($photo))
                        $q['photo'] = $photo;
                    $q = $api->CreateReport($q);
                    if ($q)
                        $err = 'La tua segnalazione &egrave; stata inviata correttamente.';
                    else
                        $err = 'Si &egrave; verificato un errore, riprovare pi&ugrave; tardi';
                }
            } while (false);
            $page->body->setVariables(array('error' => $err));
            break;
        case "report":
            if (!$id)
                $page->panic();
            $page->setTitle('Visualizza segnalazione');
            $report = $api->GetReport($id);
            if (!is_array($report))
                $page->panic();
            countFeedbacks($report);
            $error = '';
            $p = "./uploads/" . $report['photo'];
            $report['photo'] = (!empty($report['photo']) ? '<a href="' . $p . '" target="_blank"><img width="400" height="200" src="' . $p . '" /></a>' : '');
            if (isset($_GET['feedback'])) {
                $fb = $_GET['feedback'];
                if (is_numeric($fb)) {
                    $fb = (int)$fb;
                    $q = $api->ListFeedbacks(['userId' => $api->CurrentUser['id'], 'reportId' => $id]);
                    if (is_array($q) && count($q) > 0)
                        $error = 'Hai gi&agrave; dato riscontro su questa segnalazione.';
                    else {
                        $q = $api->CreateFeedback($id, $fb);
                        if ($q) {
                            if ($fb === 1)
                                $report['bother']++;
                            else
                                $report['danger']++;
                            $error = 'Il tuo riscontro &egrave; stato salvato';
                        } else
                            $error = 'Si &egrave; verificato un errore';
                    }
                }
            }
            $report['rid'] = $id;
            $report['error'] = $error;
            $page->body->setVariables($report);
            break;
        case "approve":
            if (!$isFunctionary || $isSuperUser)
                $page->panic();
            $page->setTitle('Approva segnalazioni');
            $reports = $api->ListReports(array('status' => ReportStatus::Open, 'departmentId' => $api->CurrentUser['myDepartment']));
            if (!is_array($reports))
                $page->panic();

            if (count($reports) > 0)
                $t = xPage::ToHtmlTable($reports);
            else
                $t = 'Nessuna segnalazione da approvare';
            break;
        case "profile":
            if (!$id)
                $id = $api->CurrentUser['id'];
            $page->setTitle('Profilo');
            $profile = $api->GetUserProfile(array('id' => $id));
            if (is_array($profile)) {
                $progress = ($profile['user']['reputation'] % 100);
                $level = floor($profile['user']['reputation'] / 100);
                $vars = array(
                    'name' => $profile['user']['name'],
                    'surname' => $profile['user']['surname'],
                    'percentage' => $progress,
                    'level' => $level,
                    'points' => $profile['user']['reputation'],
                    'badge' => 'Nessun badge ottenuto'
                );
                $page->body->setVariables($vars);
            } else
                $page->panic();
            break;
        case "dashboard":
            if (!$isFunctionary)
                $page->panic();
            $page->setTitle('Dashboard');

            break;
        default:

            break;
    }
    $page->setTitle('Home');
    $page->body->setVariables(array('hola' => "abcdef"));
    echo $page->getOutput();
    exit;
}
$allow = array('login', 'register');
if (!in_array($p, $allow))
    $p = 'login';
$page->loadBody($p);
$page->pushToHeadTag('<link rel="stylesheet" media="all" href="./css/login.css" />');
$page->setTitle('Login');
if ($p === 'register') {
    $page->setTitle('Registrazione');
    if (isset($_POST['name'])) {
        $filter = array('email', 'password', 'name', 'surname', 'phoneNumber', 'birthDate');
        $data = array();
        foreach ($filter as $val)
            if (!empty($_POST[$val]))
                $data[$val] = $_POST[$val];
        $r = $api->Register($data);

    }
} elseif (isset($_POST['email'])) {
    $mail = $_POST['email'];
    $pass = $_POST['password'];
    if (checkString($mail, 4) && checkString($pass, 4)) {
        if ($api->Login($mail, $pass)) {
            header("Location: ?");
            exit;
        }
    }
    $page->body->setVariables(array('error' => 'E-mail o password errati'));
}
$page->pushToBodyTag('<script src="https://google.com/recaptcha/api.js" async defer></script>');
$page->body->setVariables(array('error' => ''));
echo $page->getOutput();